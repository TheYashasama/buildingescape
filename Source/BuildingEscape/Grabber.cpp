// Copyright This Is mine.

#include "Grabber.h"
#include "Engine/World.h"
#include "DrawDebugHelpers.h"

#define OUT

// Sets default values for this component's properties
UGrabber::UGrabber()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UGrabber::BeginPlay()
{
	Super::BeginPlay();

	FindPhysicsHandleComponent();
	FindAndBindInput();
}

// Called every frame
void UGrabber::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	if (PhysicsHandle == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("No PhysicsHandleComponent"))
		return;
	}

	if (PhysicsHandle->GetGrabbedComponent())
	{
		PhysicsHandle->SetTargetLocation(GetLineTraceEnd());
	}
}

void UGrabber::FindAndBindInput()
{
	InputComponent = GetOwner()->FindComponentByClass<UInputComponent>();
	if (InputComponent)
	{
		InputComponent->BindAction("Grab", IE_Pressed, this, &UGrabber::Grab);
		InputComponent->BindAction("Grab", IE_Released, this, &UGrabber::Release);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("There should be a UInputComponent on %s, but here is none"), *GetOwner()->GetName())
	}
}

void UGrabber::FindPhysicsHandleComponent()
{
	PlayerController = GetWorld()->GetFirstPlayerController();
	PhysicsHandle = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();
	if (PhysicsHandle == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("There should be a UPHysicsHandleComponent on %s, but here is none"), *GetOwner()->GetName())
	}
}

void UGrabber::Grab()
{
	FHitResult HitResult = GetLineTraceHitResult();
	//UE_LOG(LogTemp, Error, TEXT("Trying to grab"))
	AActor* ActorHit = HitResult.GetActor();
	if (ActorHit)
	{
		//UE_LOG(LogTemp, Error, TEXT("%s Grabbed"), *ActorHit->GetName())
		PhysicsHandle->GrabComponentAtLocationWithRotation(
			HitResult.GetComponent(), 
			NAME_None, 
			GetLineTraceEnd(), FRotator(0.0f, 0.0f, 0.0f));
	} 
}

FVector UGrabber::GetPlayerViewPoint() const 
{
	FRotator PlayerViewPointRotation;
	FVector PlayerViewPointLocation;
	PlayerController->GetPlayerViewPoint(OUT PlayerViewPointLocation, PlayerViewPointRotation);
	return PlayerViewPointLocation;
}

FRotator UGrabber::GetPlayerRotation() const
{
	FVector PlayerViewPointLocation;
	FRotator PlayerViewPointRotation;
	PlayerController->GetPlayerViewPoint(PlayerViewPointLocation, OUT PlayerViewPointRotation);
	return PlayerViewPointRotation;
}

FVector UGrabber::GetLineTraceEnd() const
{
	FVector PlayerViewPointLocation = GetPlayerViewPoint();
	FRotator PlayerViewPointRotation = GetPlayerRotation();
	return PlayerViewPointLocation + PlayerViewPointRotation.Vector() * Reach;
}

FHitResult UGrabber::GetLineTraceHitResult() const
{
	FCollisionQueryParams QueryParams = FCollisionQueryParams(FName(TEXT("")), false, GetOwner());
	FHitResult HitResult;
	GetWorld()->LineTraceSingleByObjectType(OUT HitResult, GetPlayerViewPoint(), GetLineTraceEnd(),
		FCollisionObjectQueryParams(ECollisionChannel::ECC_PhysicsBody),
		QueryParams
	);
	return HitResult;
}

void UGrabber::Release()
{
	if (!PhysicsHandle) return;
	PhysicsHandle->ReleaseComponent();
}

