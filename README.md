# Building Escape
#### by Timo Neumann

#### Created for the 
## "The Unreal Engine Developer Course - Learn C++ & Make Games" 
#### course on [Udemy](https://www.udemy.com). 

Course link: https://www.udemy.com/unrealcourse/

Repository represents section 3 of the course.

Contents, cited from the courses **What will I learn** section:

  * Learn C++, the games industry standard language.
  * Develop strong and transferrable problem solving skills.
  * Gain an excellent knowledge of modern game development.
  * Learn how object oriented programming works in practice.
  * Gain a more fundamental understanding of computer operation.


---

Copyright (c) 2018 Timo Neumann

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.